今天热门的一些arxiv论文翻译，手动上传版本.

招募编辑同学，AI相关专业，硕博在读，有空维护仓库~

联系方式可以发issues，也可以发我邮箱，lyl1994@mail.ustc.edu.cn

其他同学留个star，让我知道不是在玩单机~


# arXiv_top_chinese

#### 介绍
中文版arXiv头条手动版

借着qingxu的学术版arxiv全文翻译的功能，我准备创建一个中文版的arxiv网站。

让国人不再因为语言门槛，而错失最新的科技进展。

中文版网站的功能非常简单：

1. 对arxiv网站做镜像

2. 对当天热点论文做排行榜

其他的附属功能，后期再加。

由于我目前没有完整的前后端开发技能，所以暂时先用gitee提供下载和展示的功能。

欢迎有大佬带我，一起开发这个项目，我可以付费。